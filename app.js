//
//  Copyright (c) 2019 Speroteck Inc (www.speroteck.com)
//
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var serveStatic = require('serve-static');

require('./server/mysql').init(run);
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'build')));
app.set('view engine', 'html');
app.use(bodyParser.urlencoded({extended: true}));
app.use(serveStatic(__dirname, {'*': ['index.html']}));
app.use('/', require('./server/routes/index'));

module.exports = app;

function run() {
	app.listen(9000, '0.0.0.0');
}
