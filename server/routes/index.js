//
//  Copyright (c) 2019 Speroteck Inc (www.speroteck.com)
//
var express = require('express');
var router = express.Router();

router.get('/posts', require('../posts/handle-get').handleGet);
router.get('/posts/:id', require('../posts/handle-get-by-id').handleGetById);
router.delete('/posts/:id', require('../posts/handle-delete').handleDelete);
router.put('/posts/:id', require('../posts/handle-put').handlePut);
router.post('/posts', require('../posts/handle-post').handlePost);

module.exports = router;
