var mysql = require('mysql');
var connection = mysql.createConnection({
	host: 'localhost',
	database: 'react_blog',
	user: 'root',
	password: 'root',
});

const init = (cb) => {
	connection.connect(function (err) {
		if (err) {
			console.error('Error connecting: ' + err.stack);
			return;
		}
		onDbConnect(cb);
	});
};

const getConnection = () => {
	return connection;
};

function onDbConnect(cb, err, connection) {
	if (err) throw err;
	console.log("Connected correctly to db server");
	cb(connection);
}

module.exports = {init, getConnection};