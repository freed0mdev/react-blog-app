const connection = require('../../server/mysql').getConnection();

const handlePut = (req, res) => {
	const post = req.body;
	const sql = 'UPDATE `posts` SET `title` = post.title, `body` = post.body, `comments` = post.comments WHERE `posts`.`id` = ?';
	connection.query(sql, [post.id, post.title, post.body, post.comments], (err, rows, fields) => {
		if (!err) {
			res.send(rows);
		} else {
			console.log(err);
		}

	});
};

module.exports = {handlePut};