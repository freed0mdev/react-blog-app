const connection = require('../../server/mysql').getConnection();

const handleGet = (req, res) => {
	connection.query('SELECT * FROM posts', (err, rows, fields) => {
		if (!err) {
			res.send(rows);
		} else {
			console.log(err);
		}

	});
};

module.exports = {handleGet};