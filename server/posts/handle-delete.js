const connection = require('../../server/mysql').getConnection();

const handleDelete = (req, res) => {
	const id = req.params.id;
	const sql = 'DELETE FROM posts WHERE id = ? ';
	connection.query(sql, [id], (err, rows, fields) => {
		if (!err) {
			res.send('Deleted successfully');
		} else {
			console.log(err);
		}

	});
};

module.exports = {handleDelete};