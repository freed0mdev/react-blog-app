const connection = require('../../server/mysql').getConnection();

const handleGetById = (req, res) => {
	const id = req.params.id;
	const sql = 'SELECT * FROM posts WHERE id = ?';
	connection.query(sql, [id], (err, rows, fields) => {
		if (!err) {
			res.send(rows);
		} else {
			console.log(err);
		}

	});
};

module.exports = {handleGetById};