const connection = require('../../server/mysql').getConnection();

const handlePost = (req, res) => {
	const post = req.body;
	const sql = 'INSERT INTO `posts` (`id`, `title`, `body`, `comments`) VALUES (NULL, post.title, post.body, post.comments)';
	connection.query(sql, [post.id, post.title, post.body, post.comments], (err, rows, fields) => {
		if (!err) {
			res.send('Updated successfully!');
		} else {
			console.log(err);
		}

	});
};

module.exports = {handlePost};