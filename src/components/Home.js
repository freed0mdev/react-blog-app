import React, {Component} from 'react';
import AddPost from "./post/AddPost";
import Posts from "./post/Posts";

class Home extends Component {
	render() {
		return (
			<div className="container">
				<h1 className='center blue-text'>Posts:</h1>
				<AddPost />
				<div className="row">
					<Posts />
				</div>
			</div>
		)
	}
}

export default Home;