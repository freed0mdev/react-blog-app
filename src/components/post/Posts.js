import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';

class Posts extends Component {
	render() {
		const { posts } = this.props;
		const postsList = posts.length ? (
			posts.map(post => {
				return (
					<div className="col s12" key={post.id}>
						<div className="card">
							<div className="card-image">
								<img src={'https://picsum.photos/id/' + post.id + '/1257/200'} alt='Example'/>
								<Link to={'/post/' + post.id}>
									<span className="card-title">Title: {post.title}</span>
								</Link>
							</div>
							<div className="card-content">
								<p>Content: {post.body}</p>
								<p>Author: {post.userId}</p>
							</div>
						</div>
					</div>
				)
			})
		) : (
			<p className='center'>No posts.</p>
		);

		return (
			<div className='posts'>
				{postsList}
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		posts: state.posts
	}
};

export default connect(mapStateToProps)(Posts);