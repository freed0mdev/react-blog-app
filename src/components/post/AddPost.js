import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addPost} from "../../actions/postActions";

class AddPost extends Component {
	state = {
		title: '',
		body: '',
		comments: []
	};

	handleChange = (e) => {
		this.setState({[e.target.id]: e.target.value});
	};

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.addPost(this.state);
		this.setState({
			title: '',
			body: '',
			comments: []
		})
	};

	render() {
		return (
			<div className="row">
				<form className="col s12" onSubmit={this.handleSubmit}>
					<div className="row">
						<div className="input-field col s6">
							<label htmlFor='title'>Title</label>
							<input id="title" type="text" className="validate" required
							       onChange={this.handleChange} value={this.state.title} />
						</div>
						<div className="input-field col s6">
							<label htmlFor='body'>Body</label>
							<textarea id="body" className="materialize-textarea" required
							          onChange={this.handleChange} value={this.state.body}/>
						</div>
						<div className="input-field col s6">
							<button className="waves-effect waves-light btn">Create Post</button>
						</div>
					</div>
				</form>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		posts: state.posts
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		addPost: post => dispatch(addPost(post))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);