import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deletePost, addComment, deleteComment} from "../../actions/postActions";

class Post extends Component {
	state = {
		postId: '',
		comment_text: ''
	};

	componentDidMount() {
		this.setState({
			...this.state,
			postId: this.props.post ? this.props.post['id'] : ''
		});
	}

	handleChange = (e) => {
		this.setState({...this.state, [e.target.id]: e.target.value});
	};

	handleClick = () => {
		this.props.deletePost(this.props.post.id);
		this.props.history.push('/');
	};

	handleClickComment = () => {
		this.props.deleteComment(this.props.post.id);
	};

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.addComment(this.state);
		this.setState({
			...this.state,
			comment_text: ''
		})
	};

	render() {
		const comments = this.props.post && this.props.post.comments.length ? (
			this.props.post.comments.map(comment => {
				return (
					<ul className="collection">
						<li className="collection-item avatar" key={comment.id}>
							<img src={'https://picsum.photos/id/' + comment.id + '/42'} alt="" className="circle"/>
							<span className="title">Comment ID: {comment.id}</span>
							<p>Comment TEXT: {comment.comment_text}</p>
							<a href="#!" className="secondary-content" onClick={() => {this.handleClickComment(comment.id)}}><i className="material-icons">delete</i></a>
						</li>
					</ul>
				)
			})
		) : (
			<div className="center">
				No comments yet.
			</div>
		);

		const post = this.props.post ? (
			<div className="post">
				<img src={'https://picsum.photos/id/' + this.props.post.id + '/1257/400'} alt='Example'/>
				<h4 className="center">TITLE: {this.props.post.title}</h4>
				<p> BODY: {this.props.post.body}</p>
				<div className="comments">
					{comments}
					<div className="comments__add">
						<form onSubmit={this.handleSubmit}>
							<textarea className="materialize-textarea" onChange={this.handleChange} required
							          value={this.state.comment_text} id="comment_text"
							          placeholder='Enter your comment here'/>
							<button className="btn"><i className="material-icons">send</i></button>
						</form>
					</div>
				</div>
				<div className="center">
					<button className="waves-effect waves-light btn" onClick={this.handleClick}>Delete post</button>
				</div>
			</div>
		) : (
			<div className="center">Loading post...</div>
		);

		return (
			<div className='container'>
				{post}
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	let id = ownProps.match.params['post_id'];
	return {
		post: state.posts.find(post => post.id === id)
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		deletePost: id => dispatch(deletePost(id)),
		addComment: comment => dispatch(addComment(comment)),
		deleteComment: id => dispatch(deleteComment(id)),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);