import React from 'react';
import {NavLink} from "react-router-dom";

const NavBar = () => {
	return (
		<nav className='nav-wrapper red darken-3'>
			<div className="container">
				<ul className='left'>
					{/*eslint-disable-next-line*/}
					<li><a data-target="slide-out" className="sidenav-trigger"><i className="material-icons">menu</i></a></li>
				</ul>
				<NavLink to='/' className="brand-logo">React Blog</NavLink>
			</div>
			<ul id="slide-out" className="sidenav">
				<li><NavLink to="/">Home</NavLink></li>
				<li><NavLink to="/about">About</NavLink></li>
				<li><NavLink to="/contact">Contact</NavLink></li>
			</ul>
		</nav>
	)
};

export default NavBar;