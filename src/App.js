import React, {Component} from 'react';
import NavBar from './components/NavBar';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Post from './components/post/Post';
import {BrowserRouter, Route, Switch} from "react-router-dom";


class App extends Component {
	render() {
		return (
			<BrowserRouter>
				<NavBar />
				<Switch>
					<Route exact path='/' component={Home}/>
					<Route path='/about' component={About}/>
					<Route path='/contact' component={Contact}/>
					<Route path='/post/:post_id' component={Post}/>
				</Switch>
			</BrowserRouter>
		);
	}
}

export default App;
