const addPost = post => {
	return {
		type: 'ADD_POST',
		post: post
	}
};

const deletePost = id => {
	return {
		type: 'DELETE_POST',
		id: id
	}
};

const addComment = (comment) => {
	return {
		type: 'ADD_COMMENT',
		comment: comment
	}
};

const deleteComment = (id, postId) => {
	return {
		type: 'DELETE_COMMENT',
		id: id,
		postId: postId
	}
};

export { deletePost, addPost, addComment, deleteComment };