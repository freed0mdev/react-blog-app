const initState = {
	users: [],
	posts: [
		{
			id: '1',
			title: 'ada lkd mlk ',
			body: 'dsfsdfdsf',
			comments: [{id: '22', postId: '1', comment_text: 'Coment1'}, {id: '33', postId: '1', comment_text: 'Comment 2'}]
		},
		{id: '2', title: 'wdadw', body: 'adwwadwadawd', comments: []},
		{id: '3', title: 'awddwwda', body: 'gfdsgdsgdsg', comments: []},
	]
};

const rootReducer = (state = initState, action) => {
	if (action.type === 'DELETE_POST') {
		let newPosts = state.posts.filter(post => post.id !== action.id);
		return {
			...state,
			posts: newPosts
		}
	}
	if (action.type === 'ADD_POST') {
		action.post.id = Math.floor(Math.random() * 100).toString();
		return {
			...state,
			posts: [...state.posts, action.post]
		}
	}
	if (action.type === 'ADD_COMMENT') {
		action.comment.id = Math.floor(Math.random() * 100).toString();
		let post = state.posts.find(post => post.id === action.comment.postId);
		post.comments.push(action.comment);
		let newPosts = state.posts.filter(post => post.id !== action.comment.postId);
		return {
			...state,
			posts: [...newPosts, post]
		}
	}
	if (action.type === 'DELETE_COMMENT') {
		action.comment.id = Math.floor(Math.random() * 100).toString();
		let post = state.posts.find(post => post.id === action.comment.postId);
		post.comments.push(action.comment);
		let newPosts = state.posts.filter(post => post.id !== action.comment.postId);
		return {
			...state,
			posts: [...newPosts, post]
		}
	}
	return state;
};

export default rootReducer;